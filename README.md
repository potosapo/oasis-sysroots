# oasis-sysroots

Some sysroots to use with [oasislinux](https://github.com/oasislinux/oasis).

For now x86_64 is compiled fully on clang. Till my toolchain supports aarch64 those sysroots remain dependent on gcc.

## Howto

See oasislinux-install.sh on [deploy-scripts](https://codeberg.org/Potosi/deploy-scripts)

## configuration

See config.autobuild.lua on [my oasislinux fork](https://codeberg.org/Potosi/oasis).

## TODO

- [ ] LTO
- [ ] Add aarch64 sysroot
- [ ] Add all packages in oasis